@extends('layout.table')
@section('judul')
  Edit Cast {{$cast->nama}}
@endsection

@section('content')
        <form action="/cast/{{$cast->id}}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="nama"> Nama </label>
                <input type="text" class="form-control" value="{{$cast->nama}}" name="nama" id="nama" placeholder="Masukkan Nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="umur"> Umur </label>
                <input type="text" class="form-control" name="umur" id="umur" placeholder="Masukkan Umur">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="bio"> Bio </label>
                <textarea name="bio" id="bio" class="form-control" cols="30" rows="10"> {{$cast->bio}} </textarea>
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
@endsection